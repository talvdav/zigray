pub fn range(len: usize) []const void {
    return @as([*]void, undefined)[0..len];
}

pub fn rangese(start: usize, end: usize) []const void {
    return @as([*]void, undefined)[start..end];
}
