const std = @import("std");
const rl = @import("raylib");

//*******************************************************************************************
//
//   raylib - classic game: tetris
//
//   Sample game developed by Marc Palau and Ramon Santamaria
//
//   This game has been created using raylib v1.3 (www.raylib.com)
//   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
//
//   Copyright (c) 2015 Ramon Santamaria (@raysan5)
//
//********************************************************************************************/

//----------------------------------------------------------------------------------
// Some Defines
//----------------------------------------------------------------------------------
const SQUARE_SIZE: i32 = 20;

const GRID_HORIZONTAL_SIZE: i32 = 12;
const GRID_VERTICAL_SIZE: i32 = 20;

const LATERAL_SPEED: i32 = 10;
const TURNING_SPEED: i32 = 12;
const FAST_FALL_AWAIT_COUNTER: i32 = 30;

const FADING_TIME: i32 = 33;

//----------------------------------------------------------------------------------
// Types and Structures Definition
//----------------------------------------------------------------------------------
const GridSquare = enum { EMPTY, MOVING, FULL, BLOCK, FADING };

//------------------------------------------------------------------------------------
// Global Variables Declaration
//------------------------------------------------------------------------------------
const screenWidth: i32 = 800;
const screenHeight: i32 = 450;

var gameOver: bool = false;
var pause: bool = false;

// Matrices
var grid: [GRID_HORIZONTAL_SIZE][GRID_VERTICAL_SIZE]GridSquare = undefined;
var piece: [4][4]GridSquare = undefined;
var incomingPiece: [4][4]GridSquare = undefined;

// Theese variables keep track of the active piece position
var piecePositionX: usize = 0;
var piecePositionY: usize = 0;

// Game parameters
var fadingColor: rl.Color = undefined;
//static int fallingSpeed;           // In frames

var beginPlay: bool = true; // This var is only true at the begining of the game, used for the first matrix creations
var pieceActive: bool = false;
var detection: bool = false;
var lineToDelete: bool = false;

// Statistics
var level: i32 = 1;
var lines: i32 = 0;

// Counters
var gravityMovementCounter: i32 = 0;
var lateralMovementCounter: i32 = 0;
var turnMovementCounter: i32 = 0;
var fastFallMovementCounter: i32 = 0;

var fadeLineCounter: i32 = 0;

// Based on level
var gravitySpeed: i32 = 30;

const Vec2 = struct { x: i32, y: i32 };

//------------------------------------------------------------------------------------
// Program main entry point
//------------------------------------------------------------------------------------
pub fn run() !void {
    // Initialization (Note windowTitle is unused on Android)
    //---------------------------------------------------------
    rl.initWindow(screenWidth, screenHeight, "classic game: tetris");

    InitGame();

    rl.setTargetFPS(60);
    //--------------------------------------------------------------------------------------

    // Main game loop
    while (!rl.windowShouldClose()) // Detect window close button or ESC key
    {
        // Update and Draw
        //----------------------------------------------------------------------------------
        UpdateDrawFrame();
        //----------------------------------------------------------------------------------
    }
    // De-Initialization
    //--------------------------------------------------------------------------------------
    UnloadGame(); // Unload loaded data (textures, sounds, models...)

    rl.closeWindow(); // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
}

//--------------------------------------------------------------------------------------
// Game Module Functions Definition
//--------------------------------------------------------------------------------------

// Initialize game variables
fn InitGame() void {
    // Initialize game statistics
    level = 1;
    lines = 0;

    fadingColor = rl.Color.gray;

    piecePositionX = 0;
    piecePositionY = 0;

    pause = false;

    beginPlay = true;
    pieceActive = false;
    detection = false;
    lineToDelete = false;

    // Counters
    gravityMovementCounter = 0;
    lateralMovementCounter = 0;
    turnMovementCounter = 0;
    fastFallMovementCounter = 0;

    fadeLineCounter = 0;
    gravitySpeed = 30;

    // Initialize grid matrices
    for (0..GRID_HORIZONTAL_SIZE) |i| {
        for (0..GRID_VERTICAL_SIZE) |j| {
            if ((j == GRID_VERTICAL_SIZE - 1) or (i == 0) or (i == GRID_HORIZONTAL_SIZE - 1)) {
                grid[i][j] = GridSquare.BLOCK;
            } else {
                grid[i][j] = GridSquare.EMPTY;
            }
        }
    }

    // Initialize incoming piece matrices
    for (0..4) |i| {
        for (0..4) |j| {
            incomingPiece[i][j] = GridSquare.EMPTY;
        }
    }
}

// Update game (one frame)
fn UpdateGame() void {
    if (!gameOver) {
        if (rl.isKeyPressed(rl.KeyboardKey.key_p)) {
            pause = !pause;
        }

        if (!pause) {
            if (!lineToDelete) {
                if (!pieceActive) {
                    // Get another piece
                    pieceActive = Createpiece();

                    // We leave a little time before starting the fast falling down
                    fastFallMovementCounter = 0;
                } else // Piece falling
                {
                    // Counters update
                    fastFallMovementCounter += 1;
                    gravityMovementCounter += 1;
                    lateralMovementCounter += 1;
                    turnMovementCounter += 1;

                    // We make sure to move if we've pressed the key this frame
                    if (rl.isKeyPressed(rl.KeyboardKey.key_left) or rl.isKeyPressed(rl.KeyboardKey.key_right)) lateralMovementCounter = LATERAL_SPEED;
                    if (rl.isKeyPressed(rl.KeyboardKey.key_up)) turnMovementCounter = TURNING_SPEED;

                    // Fall down
                    if (rl.isKeyDown(rl.KeyboardKey.key_down) and (fastFallMovementCounter >= FAST_FALL_AWAIT_COUNTER)) {
                        // We make sure the piece is going to fall this frame
                        gravityMovementCounter += gravitySpeed;
                    }

                    if (gravityMovementCounter >= gravitySpeed) {
                        // Basic falling movement
                        CheckDetection(&detection);

                        // Check if the piece has collided with another piece or with the boundings
                        ResolveFallingMovement(&detection, &pieceActive);

                        // Check if we fullfilled a line and if so, erase the line and pull down the the lines above
                        CheckCompletion(&lineToDelete);

                        gravityMovementCounter = 0;
                    }

                    // Move laterally at player's will
                    if (lateralMovementCounter >= LATERAL_SPEED) {
                        // Update the lateral movement and if success, reset the lateral counter
                        if (!ResolveLateralMovement()) lateralMovementCounter = 0;
                    }

                    // Turn the piece at player's will
                    if (turnMovementCounter >= TURNING_SPEED) {
                        // Update the turning movement and reset the turning counter
                        if (ResolveTurnMovement()) turnMovementCounter = 0;
                    }
                }

                // Game over logic
                for (0..2) |j| {
                    for (1..GRID_HORIZONTAL_SIZE - 1) |i| {
                        if (grid[i][j] == GridSquare.FULL) {
                            gameOver = true;
                        }
                    }
                }
            } else {
                // Animation when deleting lines
                fadeLineCounter += 1;

                if (@mod(fadeLineCounter, 8) < 4) {
                    fadingColor = rl.Color.maroon;
                } else {
                    fadingColor = rl.Color.gray;
                }

                if (fadeLineCounter >= FADING_TIME) {
                    var deletedLines: i32 = 0;
                    deletedLines = DeleteCompleteLines();
                    fadeLineCounter = 0;
                    lineToDelete = false;

                    lines += deletedLines;
                }
            }
        }
    } else {
        if (rl.isKeyPressed(rl.KeyboardKey.key_enter)) {
            InitGame();
            gameOver = false;
        }
    }
}

// Draw game (one frame)
fn DrawGame() void {
    rl.beginDrawing();

    rl.clearBackground(rl.Color.ray_white);

    if (!gameOver) {
        // Draw gameplay area
        var _offset: rl.Vector2 = undefined;
        _offset.x = screenWidth / 2 - (GRID_HORIZONTAL_SIZE * SQUARE_SIZE / 2) - 50;
        _offset.y = screenHeight / 2 - ((GRID_VERTICAL_SIZE - 1) * SQUARE_SIZE / 2) + SQUARE_SIZE * 2;

        _offset.y -= 50; // NOTE: Harcoded position!

        var offset = Vec2{ .x = @intFromFloat(_offset.x), .y = @intFromFloat(_offset.y) };

        const controller = offset.x;

        for (0..GRID_VERTICAL_SIZE) |j| {
            for (0..GRID_HORIZONTAL_SIZE) |i| {
                // Draw each square of the grid
                if (grid[i][j] == GridSquare.EMPTY) {
                    rl.drawLine(offset.x, offset.y, offset.x + SQUARE_SIZE, offset.y, rl.Color.light_gray);
                    rl.drawLine(offset.x, offset.y, offset.x, offset.y + SQUARE_SIZE, rl.Color.light_gray);
                    rl.drawLine(offset.x + SQUARE_SIZE, offset.y, offset.x + SQUARE_SIZE, offset.y + SQUARE_SIZE, rl.Color.light_gray);
                    rl.drawLine(offset.x, offset.y + SQUARE_SIZE, offset.x + SQUARE_SIZE, offset.y + SQUARE_SIZE, rl.Color.light_gray);
                    offset.x += SQUARE_SIZE;
                } else if (grid[i][j] == GridSquare.FULL) {
                    rl.drawRectangle(offset.x, offset.y, SQUARE_SIZE, SQUARE_SIZE, rl.Color.gray);
                    offset.x += SQUARE_SIZE;
                } else if (grid[i][j] == GridSquare.MOVING) {
                    rl.drawRectangle(offset.x, offset.y, SQUARE_SIZE, SQUARE_SIZE, rl.Color.dark_gray);
                    offset.x += SQUARE_SIZE;
                } else if (grid[i][j] == GridSquare.BLOCK) {
                    rl.drawRectangle(offset.x, offset.y, SQUARE_SIZE, SQUARE_SIZE, rl.Color.light_gray);
                    offset.x += SQUARE_SIZE;
                } else if (grid[i][j] == GridSquare.FADING) {
                    rl.drawRectangle(offset.x, offset.y, SQUARE_SIZE, SQUARE_SIZE, fadingColor);
                    offset.x += SQUARE_SIZE;
                }
            }

            offset.x = controller;
            offset.y += SQUARE_SIZE;
        }

        // Draw incoming piece (hardcoded)
        offset.x = 500;
        offset.y = 45;

        const controler = offset.x;

        for (0..4) |j| {
            for (0..4) |i| {
                if (incomingPiece[i][j] == GridSquare.EMPTY) {
                    rl.drawLine(offset.x, offset.y, offset.x + SQUARE_SIZE, offset.y, rl.Color.light_gray);
                    rl.drawLine(offset.x, offset.y, offset.x, offset.y + SQUARE_SIZE, rl.Color.light_gray);
                    rl.drawLine(offset.x + SQUARE_SIZE, offset.y, offset.x + SQUARE_SIZE, offset.y + SQUARE_SIZE, rl.Color.light_gray);
                    rl.drawLine(offset.x, offset.y + SQUARE_SIZE, offset.x + SQUARE_SIZE, offset.y + SQUARE_SIZE, rl.Color.light_gray);
                    offset.x += SQUARE_SIZE;
                } else if (incomingPiece[i][j] == GridSquare.MOVING) {
                    rl.drawRectangle(offset.x, offset.y, SQUARE_SIZE, SQUARE_SIZE, rl.Color.gray);
                    offset.x += SQUARE_SIZE;
                }
            }

            offset.x = controler;
            offset.y += SQUARE_SIZE;
        }

        rl.drawText("INCOMING:", offset.x, offset.y - 100, 10, rl.Color.gray);
        rl.drawText(rl.textFormat("LINES:      %04i", .{lines}), offset.x, offset.y + 20, 10, rl.Color.gray);

        if (pause) rl.drawText("GAME PAUSED", @divTrunc(screenWidth, 2) - @divTrunc(rl.measureText("GAME PAUSED", 40), 2), @divTrunc(screenHeight, 2) - 40, 40, rl.Color.black);
    } else rl.drawText("PRESS [ENTER] TO PLAY AGAIN", @divTrunc(rl.getScreenWidth(), 2) - @divTrunc(rl.measureText("PRESS [ENTER] TO PLAY AGAIN", 20), 2), @divTrunc(rl.getScreenHeight(), 2) - 50, 20, rl.Color.gray);

    rl.endDrawing();
}

// Unload game variables
fn UnloadGame() void {
    // TODO: Unload all dynamic loaded data (textures, sounds, models...)
}

// Update and Draw (one frame)
fn UpdateDrawFrame() void {
    UpdateGame();
    DrawGame();
}

//--------------------------------------------------------------------------------------
// Additional module functions
//--------------------------------------------------------------------------------------
fn Createpiece() bool {
    piecePositionX = ((GRID_HORIZONTAL_SIZE - 4) / 2);
    piecePositionY = 0;

    // If the game is starting and you are going to create the first piece, we create an extra one
    if (beginPlay) {
        GetRandompiece();
        beginPlay = false;
    }

    // We assign the incoming piece to the actual piece
    for (0..4) |i| {
        for (0..4) |j| {
            piece[i][j] = incomingPiece[i][j];
        }
    }

    // We assign a random piece to the incoming one
    GetRandompiece();

    // Assign the piece to the grid
    for (piecePositionX..piecePositionX + 4) |i| {
        for (0..4) |j| {
            if (piece[i - piecePositionX][j] == GridSquare.MOVING) grid[i][j] = GridSquare.MOVING;
        }
    }

    return true;
}

fn GetRandompiece() void {
    const random = rl.getRandomValue(0, 6);

    for (0..4) |i| {
        for (0..4) |j| {
            incomingPiece[i][j] = GridSquare.EMPTY;
        }
    }

    if (random == 0) {
        incomingPiece[1][1] = GridSquare.MOVING;
        incomingPiece[2][1] = GridSquare.MOVING;
        incomingPiece[1][2] = GridSquare.MOVING;
        incomingPiece[2][2] = GridSquare.MOVING;
    }
    //Cube
    if (random == 1) {
        incomingPiece[1][0] = GridSquare.MOVING;
        incomingPiece[1][1] = GridSquare.MOVING;
        incomingPiece[1][2] = GridSquare.MOVING;
        incomingPiece[2][2] = GridSquare.MOVING;
    }
    if (random == 2) {
        incomingPiece[1][2] = GridSquare.MOVING;
        incomingPiece[2][0] = GridSquare.MOVING;
        incomingPiece[2][1] = GridSquare.MOVING;
        incomingPiece[2][2] = GridSquare.MOVING;
    }
    if (random == 3) {
        incomingPiece[0][1] = GridSquare.MOVING;
        incomingPiece[1][1] = GridSquare.MOVING;
        incomingPiece[2][1] = GridSquare.MOVING;
        incomingPiece[3][1] = GridSquare.MOVING;
    }
    if (random == 4) {
        incomingPiece[1][0] = GridSquare.MOVING;
        incomingPiece[1][1] = GridSquare.MOVING;
        incomingPiece[1][2] = GridSquare.MOVING;
        incomingPiece[2][1] = GridSquare.MOVING;
    }
    if (random == 5) {
        incomingPiece[1][1] = GridSquare.MOVING;
        incomingPiece[2][1] = GridSquare.MOVING;
        incomingPiece[2][2] = GridSquare.MOVING;
        incomingPiece[3][2] = GridSquare.MOVING;
    }
    if (random == 6) {
        incomingPiece[1][2] = GridSquare.MOVING;
        incomingPiece[2][2] = GridSquare.MOVING;
        incomingPiece[2][1] = GridSquare.MOVING;
        incomingPiece[3][1] = GridSquare.MOVING;
    }
}

fn ResolveFallingMovement(detectionP: *bool, pieceActiveP: *bool) void {
    // If we finished moving this piece, we stop it
    if (detectionP.*) {
        var j: usize = GRID_VERTICAL_SIZE - 2;
        while (j > 0) {
            for (1..GRID_HORIZONTAL_SIZE - 1) |i| {
                if (grid[i][j] == GridSquare.MOVING) {
                    grid[i][j] = GridSquare.FULL;
                    detectionP.* = false;
                    pieceActiveP.* = false;
                }
            }
            j -= 1;
        }
    } else // We move down the piece
    {
        var j: usize = GRID_VERTICAL_SIZE - 1;
        while (j > 0) {
            j -= 1;
            for (1..GRID_HORIZONTAL_SIZE - 1) |i| {
                if (grid[i][j] == GridSquare.MOVING) {
                    grid[i][j + 1] = GridSquare.MOVING;
                    grid[i][j] = GridSquare.EMPTY;
                }
            }
        }
        piecePositionY += 1;
    }
}

fn ResolveLateralMovement() bool {
    var collision: bool = false;

    // Piece movement
    if (rl.isKeyDown(rl.KeyboardKey.key_left)) // Move left
    {
        // Check if is possible to move to left
        var j: usize = GRID_VERTICAL_SIZE - 2;
        while (j > 0) : (j -= 1) {
            for (1..GRID_HORIZONTAL_SIZE - 1) |i| {
                if (grid[i][j] == GridSquare.MOVING) {
                    // Check if we are touching the left wall or we have a full square at the left
                    if ((i - 1 == 0) or (grid[i - 1][j] == GridSquare.FULL)) {
                        collision = true;
                    }
                }
            }
        }

        // If able, move left
        if (!collision) {
            j = GRID_VERTICAL_SIZE - 2;
            while (j > 0) : (j -= 1) {
                for (1..GRID_HORIZONTAL_SIZE - 1) |i| // We check the matrix from left to right
                {
                    // Move everything to the left
                    if (grid[i][j] == GridSquare.MOVING) {
                        grid[i - 1][j] = GridSquare.MOVING;
                        grid[i][j] = GridSquare.EMPTY;
                    }
                }
            }
            piecePositionX -= 1;
        }
    } else if (rl.isKeyDown(rl.KeyboardKey.key_right)) // Move right
    {
        // Check if is possible to move to right
        var j: usize = GRID_VERTICAL_SIZE - 2;
        while (j > 0) : (j -= 1) {
            for (1..GRID_HORIZONTAL_SIZE - 1) |i| {
                if (grid[i][j] == GridSquare.MOVING) {
                    // Check if we are touching the right wall or we have a full square at the right
                    if ((i + 1 == GRID_HORIZONTAL_SIZE - 1) or (grid[i + 1][j] == GridSquare.FULL)) {
                        collision = true;
                    }
                }
            }
        }

        // If able move right
        if (!collision) {
            j = GRID_VERTICAL_SIZE - 2;
            while (j > 0) : (j -= 1) {
                var i: usize = GRID_HORIZONTAL_SIZE - 1;
                while (i >= 1) : (i -= 1) // We check the matrix from right to left
                {
                    // Move everything to the right
                    if (grid[i][j] == GridSquare.MOVING) {
                        grid[i + 1][j] = GridSquare.MOVING;
                        grid[i][j] = GridSquare.EMPTY;
                    }
                }
            }
            piecePositionX += 1;
        }
    }

    return collision;
}

fn ResolveTurnMovement() bool {
    // Input for turning the piece
    if (rl.isKeyDown(rl.KeyboardKey.key_up)) {
        var aux: GridSquare = undefined;
        var checker: bool = false;

        // Check all turning possibilities
        if ((grid[piecePositionX + 3][piecePositionY] == GridSquare.MOVING) and
            (grid[piecePositionX][piecePositionY] != GridSquare.EMPTY) and
            (grid[piecePositionX][piecePositionY] != GridSquare.MOVING))
        {
            checker = true;
        }

        if ((grid[piecePositionX + 3][piecePositionY + 3] == GridSquare.MOVING) and
            (grid[piecePositionX + 3][piecePositionY] != GridSquare.EMPTY) and
            (grid[piecePositionX + 3][piecePositionY] != GridSquare.MOVING))
        {
            checker = true;
        }

        if ((grid[piecePositionX][piecePositionY + 3] == GridSquare.MOVING) and
            (grid[piecePositionX + 3][piecePositionY + 3] != GridSquare.EMPTY) and
            (grid[piecePositionX + 3][piecePositionY + 3] != GridSquare.MOVING))
        {
            checker = true;
        }

        if ((grid[piecePositionX][piecePositionY] == GridSquare.MOVING) and
            (grid[piecePositionX][piecePositionY + 3] != GridSquare.EMPTY) and
            (grid[piecePositionX][piecePositionY + 3] != GridSquare.MOVING))
        {
            checker = true;
        }

        if ((grid[piecePositionX + 1][piecePositionY] == GridSquare.MOVING) and
            (grid[piecePositionX][piecePositionY + 2] != GridSquare.EMPTY) and
            (grid[piecePositionX][piecePositionY + 2] != GridSquare.MOVING))
        {
            checker = true;
        }

        if ((grid[piecePositionX + 3][piecePositionY + 1] == GridSquare.MOVING) and
            (grid[piecePositionX + 1][piecePositionY] != GridSquare.EMPTY) and
            (grid[piecePositionX + 1][piecePositionY] != GridSquare.MOVING))
        {
            checker = true;
        }

        if ((grid[piecePositionX + 2][piecePositionY + 3] == GridSquare.MOVING) and
            (grid[piecePositionX + 3][piecePositionY + 1] != GridSquare.EMPTY) and
            (grid[piecePositionX + 3][piecePositionY + 1] != GridSquare.MOVING))
        {
            checker = true;
        }

        if ((grid[piecePositionX][piecePositionY + 2] == GridSquare.MOVING) and
            (grid[piecePositionX + 2][piecePositionY + 3] != GridSquare.EMPTY) and
            (grid[piecePositionX + 2][piecePositionY + 3] != GridSquare.MOVING))
        {
            checker = true;
        }

        if ((grid[piecePositionX + 2][piecePositionY] == GridSquare.MOVING) and
            (grid[piecePositionX][piecePositionY + 1] != GridSquare.EMPTY) and
            (grid[piecePositionX][piecePositionY + 1] != GridSquare.MOVING))
        {
            checker = true;
        }

        if ((grid[piecePositionX + 3][piecePositionY + 2] == GridSquare.MOVING) and
            (grid[piecePositionX + 2][piecePositionY] != GridSquare.EMPTY) and
            (grid[piecePositionX + 2][piecePositionY] != GridSquare.MOVING))
        {
            checker = true;
        }

        if ((grid[piecePositionX + 1][piecePositionY + 3] == GridSquare.MOVING) and
            (grid[piecePositionX + 3][piecePositionY + 2] != GridSquare.EMPTY) and
            (grid[piecePositionX + 3][piecePositionY + 2] != GridSquare.MOVING))
        {
            checker = true;
        }

        if ((grid[piecePositionX][piecePositionY + 1] == GridSquare.MOVING) and
            (grid[piecePositionX + 1][piecePositionY + 3] != GridSquare.EMPTY) and
            (grid[piecePositionX + 1][piecePositionY + 3] != GridSquare.MOVING))
        {
            checker = true;
        }

        if ((grid[piecePositionX + 1][piecePositionY + 1] == GridSquare.MOVING) and
            (grid[piecePositionX + 1][piecePositionY + 2] != GridSquare.EMPTY) and
            (grid[piecePositionX + 1][piecePositionY + 2] != GridSquare.MOVING))
        {
            checker = true;
        }

        if ((grid[piecePositionX + 2][piecePositionY + 1] == GridSquare.MOVING) and
            (grid[piecePositionX + 1][piecePositionY + 1] != GridSquare.EMPTY) and
            (grid[piecePositionX + 1][piecePositionY + 1] != GridSquare.MOVING))
        {
            checker = true;
        }

        if ((grid[piecePositionX + 2][piecePositionY + 2] == GridSquare.MOVING) and
            (grid[piecePositionX + 2][piecePositionY + 1] != GridSquare.EMPTY) and
            (grid[piecePositionX + 2][piecePositionY + 1] != GridSquare.MOVING))
        {
            checker = true;
        }

        if ((grid[piecePositionX + 1][piecePositionY + 2] == GridSquare.MOVING) and
            (grid[piecePositionX + 2][piecePositionY + 2] != GridSquare.EMPTY) and
            (grid[piecePositionX + 2][piecePositionY + 2] != GridSquare.MOVING))
        {
            checker = true;
        }

        if (!checker) {
            aux = piece[0][0];
            piece[0][0] = piece[3][0];
            piece[3][0] = piece[3][3];
            piece[3][3] = piece[0][3];
            piece[0][3] = aux;

            aux = piece[1][0];
            piece[1][0] = piece[3][1];
            piece[3][1] = piece[2][3];
            piece[2][3] = piece[0][2];
            piece[0][2] = aux;

            aux = piece[2][0];
            piece[2][0] = piece[3][2];
            piece[3][2] = piece[1][3];
            piece[1][3] = piece[0][1];
            piece[0][1] = aux;

            aux = piece[1][1];
            piece[1][1] = piece[2][1];
            piece[2][1] = piece[2][2];
            piece[2][2] = piece[1][2];
            piece[1][2] = aux;
        }

        var j: usize = GRID_VERTICAL_SIZE - 2;
        while (j > 0) : (j -= 1) {
            for (1..GRID_HORIZONTAL_SIZE - 1) |i| {
                if (grid[i][j] == GridSquare.MOVING) {
                    grid[i][j] = GridSquare.EMPTY;
                }
            }
        }

        for (piecePositionX..piecePositionX + 4) |ii| {
            for (piecePositionY..piecePositionY + 4) |jj| {
                if (piece[ii - piecePositionX][jj - piecePositionY] == GridSquare.MOVING) {
                    grid[ii][jj] = GridSquare.MOVING;
                }
            }
        }

        return true;
    }

    return false;
}

fn CheckDetection(detectionP: *bool) void {
    var j: usize = GRID_VERTICAL_SIZE - 2;
    while (j > 0) : (j -= 1) {
        for (1..GRID_HORIZONTAL_SIZE - 1) |i| {
            if ((grid[i][j] == GridSquare.MOVING) and ((grid[i][j + 1] == GridSquare.FULL) or (grid[i][j + 1] == GridSquare.BLOCK))) detectionP.* = true;
        }
    }
}

fn CheckCompletion(lineToDeleteP: *bool) void {
    var calculator: i32 = 0;
    var j: usize = GRID_VERTICAL_SIZE - 2;
    while (j > 0) : (j -= 1) {
        calculator = 0;
        for (1..GRID_HORIZONTAL_SIZE - 1) |i| {
            // Count each square of the line
            if (grid[i][j] == GridSquare.FULL) {
                calculator += 1;
            }

            // Check if we completed the whole line
            if (calculator == GRID_HORIZONTAL_SIZE - 2) {
                lineToDeleteP.* = true;
                calculator = 0;
                // points++;

                // Mark the completed line
                for (1..GRID_HORIZONTAL_SIZE - 1) |z| {
                    grid[z][j] = GridSquare.FADING;
                }
            }
        }
    }
}

fn DeleteCompleteLines() i32 {
    var deletedLines: i32 = 0;

    // Erase the completed line
    var j: usize = GRID_VERTICAL_SIZE - 2;
    while (j > 0) : (j -= 1) {
        while (grid[1][j] == GridSquare.FADING) {
            for (1..GRID_HORIZONTAL_SIZE - 1) |i| {
                grid[i][j] = GridSquare.EMPTY;
            }
            var j2: usize = j - 1;
            while (j2 > 0) : (j2 -= 1) {
                for (1..GRID_HORIZONTAL_SIZE - 1) |ii2| {
                    if (grid[ii2][j2] == GridSquare.FULL) {
                        grid[ii2][j2 + 1] = GridSquare.FULL;
                        grid[ii2][j2] = GridSquare.EMPTY;
                    } else if (grid[ii2][j2] == GridSquare.FADING) {
                        grid[ii2][j2 + 1] = GridSquare.FADING;
                        grid[ii2][j2] = GridSquare.EMPTY;
                    }
                }
            }

            deletedLines += 1;
        }
    }

    return deletedLines;
}
