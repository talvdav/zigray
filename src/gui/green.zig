const std = @import("std");
const rl = @cImport({
    @cInclude("raylib.h");
    @cDefine("RAYGUI_IMPLEMENTATION", "1");
    @cInclude("raygui.h");
});

pub fn run() void {
    rl.InitWindow(800, 600, "Raylib");
    defer rl.CloseWindow(); // Close window and OpenGL context

    rl.SetTargetFPS(60);

    while (!rl.WindowShouldClose()) {
        rl.BeginDrawing();
        rl.ClearBackground(rl.GREEN);
        rl.EndDrawing();
    }
}
