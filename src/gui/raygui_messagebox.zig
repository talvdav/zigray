const std = @import("std");
const rl = @import("raylib");
const rg = @import("raygui");

pub fn run() void {
    const screenWidth = 1200;
    const screenHeight = 600;
    rl.initWindow(screenWidth, screenHeight, "Raygui - MessageBox Example.");
    rl.setTargetFPS(60);

    var showMessageBox: bool = false;

    while (!rl.windowShouldClose()) {
        rl.beginDrawing();
        defer rl.endDrawing();

        rl.clearBackground(rl.Color.white);

        if ((rg.guiButton(rl.Rectangle{ .x = 24, .y = 24, .width = 120, .height = 30 }, "#191#Show Message")) == 1) {
            showMessageBox = true;
        }

        if ((rg.guiButton(rl.Rectangle{ .x = 154, .y = 24, .width = 120, .height = 30 }, "#191#NICE!")) == 1) {
            sayNice();
        }
        if ((rg.guiButton(rl.Rectangle{ .x = 284, .y = 24, .width = 120, .height = 30 }, "#191#COOL!")) == 1) {
            sayCool();
        }

        if (showMessageBox) {
            const result = rg.guiMessageBox(rl.Rectangle{ .x = 85, .y = 70, .width = 250, .height = 100 }, "#191#Message Box", "Hi! This is a message!", "Nice;Cool");
            //std.debug.print("{d}\n", .{result});
            if (result == 0) {
                showMessageBox = false;
            }
            if (result == 1) {
                sayNice();
                showMessageBox = false;
            }
            if (result == 2) {
                sayCool();
                showMessageBox = false;
            }
        }
        rl.drawText("Congrats! You created your first window!", 190, 200, 20, rl.Color.light_gray);
    }
    rl.closeWindow();
}

fn sayNice() void {
    std.debug.print("NICE!\n", .{});
}

fn sayCool() void {
    std.debug.print("COOL!\n", .{});
}
