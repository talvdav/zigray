const std = @import("std");
const rl = @import("raylib");
const tetris = @import("games/tetris.zig");
const repl = @import("lua/interpreter.zig");
const zigfn = @import("lua/zig-fn.zig");

pub fn main() !void {
    const allocator = std.heap.page_allocator;
    var args = try std.process.argsWithAllocator(allocator);
    defer args.deinit();
    _ = args.skip();
    const arg = args.next();
    var start: []const u8 = "";

    if (arg == null) {
        start = "default";
    } else {
        start = arg.?;
    }

    if (std.mem.eql(u8, start, "default")) {
        std.debug.print("Hello World!\n", .{});
    }
    if (std.mem.eql(u8, start, "tetris")) {
        std.debug.print("tetris\n", .{});
        try tetris.run();
    }
    if (std.mem.eql(u8, start, "repl")) {
        std.debug.print("repl\n", .{});
        try repl.run();
    }
    if (std.mem.eql(u8, start, "zigfn")) {
        std.debug.print("zigfn\n", .{});
        try zigfn.run();
    }
    std.debug.print("\n\n\t--- FIN ---\n\n", .{});
}

test "simple test" {
    var list = std.ArrayList(i32).init(std.testing.allocator);
    defer list.deinit(); // try commenting this out and see if zig detects the memory leak!
    try list.append(42);
    try std.testing.expectEqual(@as(i32, 42), list.pop());
}
