const std = @import("std");
const testing = std.testing;

const fmt = std.fmt;
const pow = std.math.pow;
const mem = std.mem;
const debugPrint = std.debug.print;

fn getIpAsU32(ip_string: []const u8) !u32 {
    var i: u5 = 24;
    var ip: u32 = 0;
    var help: u32 = 0;
    var f = mem.splitAny(u8, ip_string, "./");

    while (f.next()) |v| {
        help = try fmt.parseInt(u8, v, 10);
        ip = ip + (help << i);
        if (i == 0) break;
        i -= 8;
    }

    return ip;
}

fn getMaskAsU32(ip_string: []const u8) !u32 {
    var h = mem.splitAny(u8, ip_string, "/");
    var ret: u32 = 0;
    var c: u8 = 0;

    while (h.next()) |v| {
        if (c == 0) {
            c += 1;
            continue;
        }
        ret = try fmt.parseInt(u8, v, 10);
    }
    return ret;
}

fn p(s: u32) void {
    std.debug.print("{d}\n", .{s});
}

fn printBitRepresentation(num: u32) !void {
    var rest: u32 = 0;
    var next: u32 = 1;
    var rep: [32]u8 = undefined;
    var bit: [1]u8 = undefined;
    var idx: u32 = 0;

    initWithZero(&rep);

    rest = num % 2;
    next = num / 2;
    _ = try fmt.bufPrint(&bit, "{d}", .{rest});
    rep[idx] = bit[0];
    idx += 1;

    while (next != 0) {
        rest = next % 2;
        next = next / 2;
        _ = try fmt.bufPrint(&bit, "{d}", .{rest});
        rep[idx] = bit[0];
        idx += 1;
    }

    idx = rep.len;
    while (idx > 0) {
        idx = idx - 1;
        debugPrint("{c}", .{rep[idx]});
        if ((idx) % 8 == 0 and (idx > 0)) {
            debugPrint("{s}", .{"."});
        }
    }
    debugPrint("{s}\n", .{""});
}

fn initWithZero(buff: *[32]u8) void {
    var i: u32 = 0;
    while (i < 32) {
        buff[i] = '0';
        i += 1;
    }
}

fn maxClients(mask: u32) u32 {
    return pow(u32, 2, 32 - mask);
}

pub fn printStuff(ip_string: []const u8) !void {
    debugPrint("ip_string: {s}\n", .{ip_string});

    const index = mem.lastIndexOf(u8, ip_string, "/");
    debugPrint("index: {d}\n", .{index.?});

    const ip = ip_string[0..index.?];
    debugPrint("ip: {s}\n", .{ip});

    const mask = ip_string[index.? + 1 ..];
    debugPrint("mask: {s}\n", .{mask});

    const mask_count = try fmt.parseInt(u32, mask, 10);
    debugPrint("mask count: {d}\n", .{mask_count});

    const mask_bit_size = 32 - mask_count;
    debugPrint("mask size: {d}\n", .{mask_bit_size});

    const max_clients = pow(u32, 2, 32 - mask_count);
    debugPrint("max_clients: {d}\n", .{max_clients});
}

fn range(len: usize) []const void {
    return @as([*]void, undefined)[0..len];
}

export fn add(a: i32, b: i32) i32 {
    return a + b;
}

test "basic add functionality" {
    try testing.expect(add(3, 7) == 10);
}
